GO_VERSION_MIN="1.21"
ENV_TAGS=("dev" "non-prod" "prod")
COMMANDS=("build" "test" "run" "clean" "deploy" "undeploy" "iterate" "open")

BASE_DIR="$(pushd $(dirname ${0}) | cut -d ' ' -f 1)"
BASE_DIR="${BASE_DIR/\~/${HOME}}"
PROJECT="$(basename ${BASE_DIR})"

function contains { local i; for i in "${@:2}"; do [[ "${1}" = "${i}" ]] && return 0; done; return 1; }
function str_join { local d="${1}" o i; for i in "${@:2}"; do [[ -z "${o}" ]] && o="${i}" || o+="${d}${i}"; done; echo "${o}"; }
function log_info { echo -e "[INFO][${FUNCNAME[1]}] ${@}"; }
function log_error { (>&2 echo -e "[\e[31mERROR\e[0m][${FUNCNAME[1]}] ${@}"); }
function version_gt {
    local l_major=$(echo "${1}" | cut -d "." -f 1); [[ -z "${l_major}" ]] && l_major=0 || l_major=$(echo "${l_major}" | bc)
    local l_minor=$(echo "${1}" | cut -d "." -f 2); [[ -z "${l_minor}" ]] && l_minor=0 || l_minor=$(echo "${l_minor}" | bc)
    local l_patch=$(echo "${1}" | cut -d "." -f 3); [[ -z "${l_patch}" ]] && l_patch=0 || l_patch=$(echo "${l_patch}" | bc)
    local r_major=$(echo "${2}" | cut -d "." -f 1); [[ -z "${r_major}" ]] && r_major=0 || r_major=$(echo "${r_major}" | bc)
    local r_minor=$(echo "${2}" | cut -d "." -f 2); [[ -z "${r_minor}" ]] && r_minor=0 || r_minor=$(echo "${r_minor}" | bc)
    local r_patch=$(echo "${2}" | cut -d "." -f 3); [[ -z "${r_patch}" ]] && r_patch=0 || r_patch=$(echo "${r_patch}" | bc)
    if [[ "${l_major}" -gt "${r_major}" ]]; then return 0
    elif [[ "${l_major}" -lt "${r_major}" ]]; then return 1
    elif [[ "${l_minor}" -gt "${r_minor}" ]]; then return 0
    elif [[ "${l_minor}" -lt "${r_minor}" ]]; then return 1
    elif [[ "${l_patch}" -gt "${r_patch}" ]]; then return 0
    else return 1; fi
}

function build {
    log_info "Running \"go build -o bin/main\""
    go build -o "bin/main"
}

function run {
    if [[ -f "${BASE_DIR}/bin/main" ]]; then
        log_info "Running \"${BASE_DIR}/bin/main\""
        ${BASE_DIR}/bin/main
    else
        log_info "Running \"go run ${BASE_DIR}/main.go\""
        go run "${BASE_DIR}/main.go"
    fi
}

function test {
    log_info "Running \"go test ./... -coverprofile=coverage.out\""
    go test ./... -coverprofile=coverage.out
    log_info "Running \"go tool cover -html=coverage.out\""
    go tool cover -html=coverage.out
}

function clean {
    local items_to_clean=()
    items_to_clean+=("${BASE_DIR}/coverage.out")
    items_to_clean+=("${BASE_DIR}/bin")
    items_to_clean+=("${BASE_DIR}/${PROJECT}")
    local item; for item in "${items_to_clean[@]}"; do [[ -e "${item}" ]] && log_info "Cleaning ${item}" && rm -rf "${item}"; done 
}

function sanity_check {
    local go_vers=$(go version | cut -d "o" -f 4 | cut -d " " -f 1)
    version_gt "${GO_VERSION_MIN}" "${go_vers}" && log_error "Min go version: \"${GO_VERSION_MIN}\", found \"${go_vers}\"" && return 1
    return 0
}

function ez {
    if [[ -z "${1}" ]] || contains "${1}" "-h" "--help"; then
        echo -e "\n  ez.sh [COMMAND] [ARGUMENTS]\n  Commands: [$(str_join ', ' ${COMMANDS[@]})]\n"
    elif contains "${1}" "${COMMANDS[@]}"; then
        sanity_check || return 1
        ${1} "${@:2}"
    else
        log_error "Unsupported command: \"${1}\""
    fi
}

ez "${@}"
