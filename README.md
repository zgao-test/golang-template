# Go
## Build
```bash
go build
go build -o "bin/main"
```
## Run
```bash
go run main.go
bin/main
```
## Test
```bash
go test ./... -coverprofile=coverage.out
go tool cover -html=coverage.out
```
# Bazel
## Clean
```bash
bazel clean
```
## Build
```bash
bazel build //...
```
## Test
```bash
bazel test //...
```
## Run
```bash
bazel run //:main
```