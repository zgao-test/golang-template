package utils

import (
	"testing"
)

func TestHi(t *testing.T) {
	got := Hi()
	want := "Hi"
	if got != want {
		t.Errorf(`got %q, want %q`, got, want)
	}
}
